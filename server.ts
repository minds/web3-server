/**
 * @author Ben Hayward
 */

import 'dotenv/config';

import express from 'express';
const app = express();

import cors from 'cors';
import { debug } from './src/common/utils';
import { router as txRoutes } from './src/routes/tx';
import { router as signRoutes } from './src/routes/sign';
import { router as withdrawRoutes } from './src/routes/withdraw';
import bodyParser from 'body-parser';
import swaggerUI from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';

const port = process.env.PORT || 3333;
const swaggerSpecs = swaggerJsDoc({
  definition: {
    openapi: '3.0.1',
    info: {
      openapi: '3.0.1',
      info: {
        title: 'Minds Web3 Server',
        version: '1.0.0'
      }
    }
  },
  apis: ['./src/routes/*.ts']
});

require('dotenv').config();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.json()); // any request coming in, transfer all body into JSON
app.use(cors()); // allow cross origin from client localhost

app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.use('/tx', txRoutes);
app.use('/sign', signRoutes);
app.use('/withdraw', withdrawRoutes);

/**
 * Health check endpoint
 * @returns { Object } status of 200, readyState of 1.
 */
app.get('/', (req, res) => {
  res.send({
    status: 200,
    readyState: 1
  });
});

/**
 * Sets app to listen on set port.
 */
app.listen(port, () => {
  debug('Server running on port ' + port);
});
