FROM mhart/alpine-node:12.22

COPY . .

CMD node dist/server.js
