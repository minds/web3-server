import express from 'express';
import { debug } from '../common/utils';
import { ProviderManager } from '../common/providerManager';
import * as ethers from 'ethers';
import { grantAccess } from '../common/authenticate';

const router: any = express.Router();
const providerManager: ProviderManager = ProviderManager.getInstance();

// Authentication middleware
router.use('/', grantAccess('general'));

/**
 * @swagger
 * /tx/encodeFunctionData:
 *   post:
 *     summary: Encode transaction data
 *     tags: [tx]
 *     produces:
 *       - application/json
 
 *       - name: params
 *         in: formData
 *         required: true
 *         type: array
 *         items:
 *           type: string
 *         description: Params to encode.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               functionSignature:
 *                 required: true
 *                 type: string
 *                 description: Function signature
 *                 example: balanceOf(address)
 *               params:
 *                 required: true
 *                 type: array
 *                 items:
 *                   type: string
 *                 example: ["0xb4Ea99EA800E5f59fBA5e342aA3a1A07cB59A074"]
 *     responses:
 *       200:
 *         description: Requested transaction data
 *       400:
 *         description: Missing parameters
 *       401:
 *         description: Missing authentication headers
 *       500:
 *         description: Internal server error
 */
router.post(
  '/encodeFunctionData',
  async (req: express.Request, res: express.Response) => {
    try {
      const functionSignature = req.body.functionSignature;
      let params = req.body.params;

      if (!functionSignature || !params) {
        return res.json({
          status: 400,
          message: 'Missing required parameters'
        });
      }

      const functionName = functionSignature.split('(')[0];
      const abi = [`function ${functionSignature}`];

      const abiInterface = new ethers.utils.Interface(abi);

      const encodedFunctionData = abiInterface.encodeFunctionData(
        functionName,
        params
      );

      if (encodedFunctionData && encodedFunctionData.length > 0) {
        debug('Encoded function data: ' + encodedFunctionData);
        return res.json({
          status: 200,
          data: encodedFunctionData
        });
      } else {
        throw new Error('An unknown error occurred encoding transaction data');
      }
    } catch (e) {
      console.error(e);
      return res.json({
        status: 500,
        message: e
      });
    }
  }
);

/**
 * @swagger
 * /tx/{txid}:
 *   get:
 *     summary: Get transaction by txid
 *     tags: [tx]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: txid
 *         in: path
 *         required: true
 *         type: string
 *         description: Transaction ID to get information for
 *     responses:
 *       200:
 *         description: Requested transaction data
 *       401:
 *         description: Missing authentication headers
 *       404:
 *         description: Transaction not found
 *       500:
 *         description: Internal server error
 */
router.get('/:txid', async (req: express.Request, res: express.Response) => {
  try {
    const txid = req.params.txid;
    const provider = providerManager.getProvider(
      req.header('X-ETH-NETWORK') ?? 'mainnet'
    );
    const tx = await provider.getTransaction(txid);

    debug('Looked up tx:', tx);

    if (!tx || Object.keys(tx).length === 0) {
      return res.json({
        status: 404,
        message: 'Transaction not found.'
      });
    }

    return res.json({
      status: 200,
      data: tx
    });
  } catch (e) {
    console.error(e);
    return res.json({
      status: 500,
      message: e
    });
  }
});

export { router };
