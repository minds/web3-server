import express from 'express';
import { grantAccess } from '../common/authenticate';
import { debug } from '../common/utils';
import { WalletManager } from '../common/walletManager';

const router: any = express.Router();
const walletManager: WalletManager = WalletManager.getInstance();

// Authentication middleware
router.use('/', grantAccess('general'));

/**
 * @swagger
 * components:
 *   schemas:
 *     Transaction:
 *       type: object
 *       properties:
 *         from:already instantiated
 */

/**
 * @swagger
 * /sign/message:
 *   post:
 *     summary: Sign a message
 *     tags: [sign]
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               message:
 *                 type: string
 *     responses:
 *       200:
 *         description: Signed message
 *       400:
 *         description: Missing parameters
 *       401:
 *         description: Missing authentication headers
 *       500:
 *         description: Internal server error
 */
router.post('/message', async (req: express.Request, res: express.Response) => {
  try {
    const message = req.body.message;

    if (!message) {
      return res.json({
        status: 400,
        message: 'Missing required parameters'
      });
    }

    const signedMessage = await walletManager
      .getWallet(
        'general',
        'mainnet' // network shouldn't matter here.
      )
      .signMessage(message);

    if (signedMessage) {
      debug('Signed message: ' + signedMessage);
      return res.json({
        status: 200,
        data: signedMessage
      });
    } else {
      throw new Error('An unknown error occurred signing the message');
    }
  } catch (e) {
    console.error(e);
    return res.json({
      status: 500,
      message: e
    });
  }
});

/**
 * @swagger
 * /sign/tx:
 *   post:
 *     summary: Sign a transaction
 *     tags: [sign]
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Transaction'
 *     responses:
 *       200:
 *         description: Signed message
 *       400:
 *         description: Missing parameters
 *       401:
 *         description: Missing authentication headers
 *       500:
 *         description: Internal server error
 */
router.post('/tx', async (req: express.Request, res: express.Response) => {
  try {
    let transaction: any = {
      to: req.body.to || null,
      gasLimit: req.body.gasLimit || null,
      gasPrice: req.body.gasPrice || null,
      nonce: req.body.nonce || null
    };

    if (req.body.from) {
      transaction.from = req.body.from;
    }

    if (req.body.data) {
      transaction.data = req.body.data;
    }

    // debug(transaction)
    if (
      !transaction.to ||
      !transaction.gasLimit ||
      !transaction.gasPrice ||
      !transaction.nonce
    ) {
      return res.json({
        status: 400,
        message: 'Missing required parameters'
      });
    }

    const signedTransaction = await walletManager
      .getWallet(
        'general',
        'mainnet' // network shouldn't matter here.
      )
      .signTransaction(transaction);

    if (signedTransaction) {
      debug('Signed transaction: ' + signedTransaction);
      return res.json({
        status: 200,
        data: signedTransaction
      });
    } else {
      throw new Error('An unknown error occurred signing the transaction');
    }
  } catch (e) {
    console.error(e);
    return res.json({
      status: 500,
      message: e
    });
  }
});

export { router };
