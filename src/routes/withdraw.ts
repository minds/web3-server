import express from 'express';
import { debug } from '../common/utils';
import { ProviderManager } from '../common/providerManager';
import { WalletManager } from '../common/walletManager';
import { ethers } from 'ethers';
import { withdrawAbi, withdrawAddress } from '../abi/withdraw';
import { grantAccess } from '../common/authenticate';

const router: any = express.Router();
const walletManager: WalletManager = WalletManager.getInstance();
const providerManager: ProviderManager = ProviderManager.getInstance();

// Authentication middleware
router.use('/', grantAccess('withdraw'));

/**
 * @swagger
 * /withdraw/complete:
 *   post:
 *     summary: Approve / complete withdrawal
 *     tags: [withdraw]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: requester
 *         in: formData
 *         required: true
 *         type: string
 *         description: The requesting address.
 *       - name: user_guid
 *         in: formData
 *         required: true
 *         type: string
 *         description: GUID of the user we are requesting for.
 *       - name: gas
 *         in: formData
 *         required: true
 *         type: string
 *         description: the amount of gas deposited.
 *       - name: amount
 *         in: formData
 *         required: true
 *         type: string
 *         description: the amount of tokens being approved for withdrawal.
 *     responses:
 *       200:
 *         description: Requested transaction data
 *       400:
 *         description: Missing parameters
 *       401:
 *         description: Missing authentication headers
 *       500:
 *         description: Internal server error
 */
router.post('/complete', async (req, res) => {
  try {
    const requester = req.body.requester;
    const userGuid = req.body.user_guid;
    const gas = req.body.gas;
    const amount = req.body.amount;
    const network = req.header('X-ETH-NETWORK') ?? 'mainnet';

    const walletSigner = walletManager
      .getWallet('withdraw', network)
      .connect(providerManager.getProvider(network));

    const withdrawContract = new ethers.Contract(
      withdrawAddress,
      withdrawAbi,
      walletSigner
    );

    const result = await withdrawContract.complete(
      requester,
      userGuid,
      gas,
      amount,
      { gasLimit: 87204 }
    );

    debug('Withdraw requested:', result);

    return res.json({
      status: 200,
      data: result['hash']
    });
  } catch (e) {
    console.error(e);
    return res.json({
      status: 500,
      message: e
    });
  }
});

export { router };
