import * as ethers from 'ethers';
import { Provider } from '@ethersproject/abstract-provider';
import { SupportedNetwork } from './types';

// array of providers.
type ProvidersArray = {
  [key in SupportedNetwork]: Provider;
};

/**
 * Singleton class for managing providers. Can be used to access
 * be instantiated providers on different networks.
 */
export class ProviderManager {
  // instance of class.
  private static _instance: ProviderManager;

  // array of wallets instantiated on initial construction.
  private providers: ProvidersArray;

  private constructor() {
    this.providers = {
      mainnet: new ethers.providers.InfuraProvider(
        'mainnet',
        process.env.RINKEBY_PROJECT_ID
      ),
      rinkeby: new ethers.providers.InfuraProvider(
        'rinkeby',
        process.env.RINKEBY_PROJECT_ID
      )
    };
  }

  /**
   * Gets instance of class, or creates a new one if one does not already exist.
   * @returns { WalletManager }
   */
  static getInstance(): ProviderManager {
    if (this._instance) {
      return this._instance;
    }

    this._instance = new ProviderManager();
    return this._instance;
  }

  /**
   * Gets provider based on requested network.
   * @param { string } network - Network to get provider for.
   * @returns { Provider } - Provider for requested network.
   * @throws { Error } - If no provider is found for the network.
   */
  public getProvider(network: string): Provider {
    switch (network) {
      case 'rinkeby':
        return this.providers.rinkeby;
      case 'mainnet':
        return this.providers.mainnet;
      default:
        throw new Error(`No provider for network ${network}`);
    }
  }
}
