import * as ethers from 'ethers';
import { NonceManager } from '@ethersproject/experimental';
import { StoredWallet, SupportedNetwork, WalletName } from './types';
import { ProviderManager } from './providerManager';

// array of wallets.
type WalletsArray = {
  [key in WalletName]?: WalletNetworksArray;
};

// array of wallet networks.
type WalletNetworksArray = {
  [key in SupportedNetwork]: NonceManager;
};

const providerManager: ProviderManager = ProviderManager.getInstance();

/**
 * Singleton class for managing wallets. Can retrieve wallet keys from environmental variables
 * And be used to access different ethers wallets for different networks - wrapped in NonceManagers.
 */
export class WalletManager {
  // instance of class.
  private static _instance: WalletManager;

  // array of wallets instantiated on initial construction.
  private wallets: WalletsArray = {};

  private constructor() {
    this.setupWithdrawWallet();
    this.setupGeneralWallet();
  }

  /**
   * Gets instance of class, or creates a new one if one does not already exist.
   * @returns { WalletManager }
   */
  static getInstance(): WalletManager {
    if (this._instance) {
      return this._instance;
    }

    this._instance = new WalletManager();
    return this._instance;
  }

  /**
   * Gets keys for a wallet from env.
   * @param { WalletName } key - key which wallet key to get (e.g. general, withdraw).
   * @returns { StoredWallet } associated stored wallet.
   * @throws { Error } if key is not set in env or if JSON is not parsable.
   */
  public getWalletKeys(key: WalletName): StoredWallet {
    try {
      const wallets = {
        general: {
          xpub: process.env.GENERAL_WALLET_XPUB,
          xpriv: process.env.GENERAL_WALLET_XPRIV
        },
        withdraw: {
          xpub: process.env.WITHDRAW_WALLET_XPUB,
          xpriv: process.env.WITHDRAW_WALLET_XPRIV
        }
      }

      if (!wallets || !wallets[key]) {
        throw new Error('Wallet not found');
      }
      return wallets[key];
    } catch (e) {
      return null;
    }
  }

  /**
   * Gets the already instantiated ethers wallet for a given wallet key and network,
   * wrapped in a nonce manager.
   * @param { WalletName } walletKey - the wallet key we are requesting the wallet for, e.g. 'general', 'withdraw'.
   * @param { SupportedNetwork } network - network we are requesting wallet for.
   * @returns { NonceManager } - Nonce manager wrapped around wallet.
   */
  public getWallet(
    walletKey: WalletName,
    network: SupportedNetwork
  ): NonceManager {
    switch (walletKey) {
      case 'withdraw':
        return this.getWithdrawWallet(network);
      case 'general':
        return this.getGeneralWallet(network);
      default:
        throw new Error(`No wallet for ${walletKey}`);
    }
  }

  /**
   * Get withdraw wallet by network.
   * @param { SupportedNetwork } network - network to get wallet by.
   * @returns { NonceManager } - Nonce manager wrapped around wallet.
   */
  private getWithdrawWallet(network: SupportedNetwork): NonceManager {
    switch (network) {
      case 'mainnet':
        return this.wallets.withdraw.mainnet;
      case 'rinkeby':
        return this.wallets.withdraw.rinkeby;
      default:
        throw new Error(`Withdraw wallet not set up for network: ${network}`);
    }
  }

  /**
   * Get general wallet by network.
   * @param { SupportedNetwork } network - network to get wallet by.
   * @returns { NonceManager } - Nonce manager wrapped around wallet.
   */
  private getGeneralWallet(network: SupportedNetwork): NonceManager {
    switch (network) {
      case 'mainnet':
        return this.wallets.general.mainnet;
      case 'rinkeby':
        return this.wallets.general.rinkeby;
      default:
        throw new Error(`General wallet not set up for network: ${network}`);
    }
  }

  /**
   * Sets up withdraw wallet in local this.wallets.
   * @returns { void }
   */
  private setupWithdrawWallet(): void {
    const walletKeys = this.getWalletKeys('withdraw');
    const withdrawXpriv = walletKeys && walletKeys['xpriv'] ? 
      walletKeys['xpriv'] :
      '';

    if (withdrawXpriv) {
      this.wallets.withdraw = {
        mainnet: new NonceManager(
          new ethers.Wallet(
            withdrawXpriv,
            providerManager.getProvider('mainnet')
          )
        ),
        rinkeby: new NonceManager(
          new ethers.Wallet(
            withdrawXpriv,
            providerManager.getProvider('rinkeby')
          )
        )
      }
      return;
    }
    console.error('Could not set up withdraw wallet');
  }

  /**
   * Sets up general wallet in local this.wallets.
   * @returns { void }
   */
  private setupGeneralWallet(): void {
    const walletKeys = this.getWalletKeys('general');
    const generalXpriv = walletKeys && walletKeys['xpriv'] ? 
      walletKeys['xpriv'] :
      '';

    if (generalXpriv) {
      this.wallets.general = {
        mainnet: new NonceManager(
          new ethers.Wallet(
            generalXpriv,
            providerManager.getProvider('mainnet')
          )
        ),
        rinkeby: new NonceManager(
          new ethers.Wallet(
            generalXpriv,
            providerManager.getProvider('rinkeby')
          )
        )
      }
      return;
    }
    console.error('Could not set up general wallet');
  }
}
