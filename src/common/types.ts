import { NonceManager } from '@ethersproject/experimental';

// Wallet stored in our env with xpub (master public key) and xpriv (master private key).
export type StoredWallet = {
  xpub: string;
  xpriv: string;
};

// Different keys for indicating which wallet we want to use / are using.
export type WalletName = 'withdraw' | 'general';

// Supported Ethereum networks - when adding new networks support must be added to relevant classes.
export type SupportedNetwork = 'mainnet' | 'rinkeby';
