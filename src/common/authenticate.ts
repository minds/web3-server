import express from 'express';
import crypto from 'crypto';
import { WalletManager } from './walletManager';
import { WalletName } from './types';

/**
 * Authentication middleware function.
 */
export const grantAccess = function (
  walletKey: WalletName
): (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => Promise<void> {
  return async (req, res, next) => {
    const walletManager = WalletManager.getInstance();
    const serverWalletKeys = walletManager.getWalletKeys(walletKey);

    if (!serverWalletKeys.xpriv || !serverWalletKeys.xpub) {
      return next(
        res.json({
          status: 500,
          message: 'Missing requested wallet'
        })
      );
    }

    if (
      !req.header('X-AUTH-KEY') ||
      !req.header('X-WALLET-ADDRESS')
    ) {
      return next(
        res.json({
          status: 401,
          message: 'Unauthorized'
        })
      );
    }

    // encode xpriv from env.
    const serverEncodedXpriv = Buffer.from(
      crypto
        .createHmac('SHA256', process.env.WALLET_ENCRYPTION_KEY)
        .update(serverWalletKeys.xpriv)
        .digest('hex')
    ).toString('base64');

    // check if they match headers - authenticate if they do.
    if (
      req.header('X-WALLET-ADDRESS') === serverWalletKeys.xpub &&
      req.header('X-AUTH-KEY') === serverEncodedXpriv
    ) {
      return next();
    } else {
      return next(
        res.json({
          status: 401,
          message: 'Unauthorized'
        })
      );
    }
  };
};
