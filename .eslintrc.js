/* eslint-disable */
module.exports = {
  env: {
    browser: true,
    es2018: true,
  },
  extends: [
    'prettier'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module',
  },
  plugins: [
    'prettier'
  ],
  rules: {
    'prettier/prettier': [
      1,
      {
        trailingComma: 'es5',
        singleQuote: true,
        semi: ["error", "always"],
      }
    ],
  },
}
